//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
#include<stdlib.h>
void main()
{
    float h,b,d,v, res;
    printf("Enter the values of h,d,b respectively.");
    scanf("%f%f%f",&h,&d,&b);
    if(b==0)
        {	
	        printf("b value cannot be 0.");
	        exit(0);
        }
	else
	    {
		    v = ((h * d * b) + ( d / b))/3;
		    printf("The volume of Tromboloid is: %f",v);
	    }
}