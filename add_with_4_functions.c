//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

int add(int *x, int *y)
{
    int res;
    res = *x+*y;
    return res;
}

int read()
{
    int x;
    printf("Enter number: ");
    scanf("%d", &x);
    return x;
}

void display(int res)
{
    printf("The sum of two numbers is: %d", res);
}

void main()
{
    int res, num1, num2;
    num1=read();
    num2=read();
    res=add(&num1, &num2);
    display(res);
}