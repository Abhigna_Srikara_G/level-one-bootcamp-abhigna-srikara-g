//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct point
{
	float x;
	float y;
};
typedef struct point Point;
Point input()
{
    Point p;
    printf("Enter the x coordinates: ");
    scanf("%f",&p.x);
	printf("Enter the y coordinates: ");
	scanf("%f",&p.y);
	return p;
}

float calculate(Point p1,Point p2)
{
	float distance;
	distance=sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
	return distance;
}

void display(Point p1,Point p2,float d)
{
	printf("Distance between (%f,%f) and (%f,%f) is %f",p1.x,p1.y,p2.x,p2.y,d);
}

int main()
{
	float d;
	Point p1,p2;
	p1=input();
	p2=input();
	d=calculate(p1,p2);
	display(p1,p2,d);
	return 0;
}
