//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>

int vol(int *h,int *b,int *d)
{
    int v;
    v=((*h**d**b)+(*d / *b))/3;
    return  v;
}

int read()
{
    int x;
    printf("Enter the number: ");
    scanf("%d",&x);
    return x;
}

void display(int v)
{
    printf("The volume of tromboloid is: %d", v);
}
void main()
{
    int h,b,d,v;
    printf("enter the values as h,b,d respectively");
    printf("\n");
    h=read();
    b=read();
    d=read();
    v=vol(&h,&b,&d);
    display(v);
}