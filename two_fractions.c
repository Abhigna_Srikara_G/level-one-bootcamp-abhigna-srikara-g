//WAP to find the sum of two fractions.
#include<stdio.h>

struct fraction
{
    int num;
    int dem;
};
typedef struct fraction Fract;

Fract input()
{
    Fract x;
    printf("Enter the numerator: ");
    scanf("%d",&x.num);
    printf("Enter the denominator: ");
    scanf("%d",&x.dem);
    return x;
}

int gcd(int m,int n)
{
    if(m==n)
        return m;
    
    if(m>n)
        return  gcd(m-n,n);
    if(m<n)
        return gcd(m,n-m);
}

Fract addfraction(Fract f1,Fract f2,Fract f3)
{
    f3.dem=gcd(f1.dem,f2.dem);
// LCM*GCD=m*n
    f3.dem=(f1.dem*f2.dem)/f3.dem;
    f3.num=(f1.num)*(f3.dem/f1.dem)+(f2.num)*(f3.dem/f2.dem);
    return f3;
}

void output(Fract f1,Fract f2,Fract f3)
{
    printf("%d/%d + %d/%d = %d/%d",f1.num,f1.dem,f2.num,f2.dem,f3.num,f3.dem);
}


int main()
{
    Fract f1,f2,f3;
    f1= input();
    f2= input();
    f3= addfraction(f1,f2,f3);
    output(f1,f2,f3);
    return(0);
}