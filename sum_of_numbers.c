//Write a program to find the sum of n different numbers using 4 functions

#include<stdio.h>
int calc()
{
    int i,n, sum=0,num;
    printf("Enter the value of n: ");
    scanf("%d", &n);
    printf("Enter the numbers:\n");
    for(i=1;i<=n;i++)
    {	
		scanf("%d", &num);
		sum+=num;
    }
    return sum;
}
void main()
{
    int result;
    result = calc();
    printf("Sum = %d",result);
}

